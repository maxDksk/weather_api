** General information**
------

- I've used Tox, PyTest and pytest-variables for settings;
- JSON schema checks, schemas located on a ./schemas folder 
- All dependencies managed by tox and virtualenv
- On ```helpers.py``` located implementation of some of API methods like search by woeid, location, etc and some helpers function for checks JSON schema

**Table of Contents**
---


[TOC]

**How to start tests**
----

All dependencies managed by ```tox``` and ```virtualenv```.


**Install Tox**

Linux/MAC

> `$ pip install tox`


**Start tests**

> `$ tox`


**General info about implementation**
-----
JSON schemas for responses located on  .```/schemas folder``` 



On **settings.json** - ```url``` variable correspond for API url. All variables from settings.json avaliable on test in ```variables``` dict
Command for pass settings to a test ```--variables setting_file_name.json```
Example:

> `$  pytest --variables settings.json`

**Exampe of usage:**

```
#!python
#
@pytest.mark.parametrize("city, latt_long, woeid", [("san", "37.777119, -122.41964",  2487956)])
def test_sf(variables, city, latt_long, woeid):

```


