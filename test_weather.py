import pytest
import helpers
from pyassert import *



@pytest.mark.parametrize("city, latt_long, woeid", [("san", "37.777119, -122.41964",  2487956)])
def test_sf(variables, city, latt_long, woeid):
    """
    1. Search for weather by "san"
    1.Check that response json schema is valid for get city by name response


    """
    url = variables.get("url")
    sf_by_location_resp = helpers.search_by_location(url, city)
    for city in sf_by_location_resp.json():
        # Check validity of json schema for responses
        helpers.assert_valid_schema(city, 'location_schema.json')

    sf_weather_list = list(filter(lambda city: city.get("title") == "San Francisco", sf_by_location_resp.json()))
    # Check that weather for San Francisco in response
    assert_that(sf_weather_list).is_not_empty()
    # Check that latt long is appropriate for San Francisco in responsetox
    assert_that(sf_weather_list[0].get("latt_long")).is_equal_to(latt_long)

    sf_by_woeid_resp = helpers.search_by_woeid(url, woeid)
    # Check that response status code is 200 OK
    assert_that(sf_by_location_resp.status_code).is_equal_to(200)
    assert_that(sf_by_woeid_resp.status_code).is_equal_to(200)
    # Check that response json schema is valid for get city by woeid response
    helpers.assert_valid_schema(sf_by_woeid_resp.json(), 'woeid_schema.json')
    sf_consolidated_weather = sf_by_woeid_resp.json().get("consolidated_weather")

    # Sort list of consolidated weather for a city by predictability and get the most predictable
    expected_weather = sorted(sf_consolidated_weather, key=lambda i: i.get("predictability"), reverse=True)[0]

    print("Expected weather in San-francisco is {}, Wind Speed is {} and Temperature is {}".format(expected_weather.get("weather_state_name"),
                                                                                                   expected_weather.get("wind_speed"),
                                                                                                   expected_weather.get("the_temp")))




@pytest.mark.parametrize("city, woeid, date_time", [("Bucharest", 868274, "2017/5/17 14:23")])
def test_bucharest(variables, city, woeid, date_time):
    url = variables.get("url")
    date = date_time.split(" ")[0]
    weather_for_time = helpers.get_weather_by_time(helpers.search_by_location_date(url, woeid, date).json(), date_time)
    #Check if weather for specified time returned
    assert_that(weather_for_time).is_not_empty()

    print("Expected weather in {} is {}, Wind Speed is {} and Temperature is {}".format(
        city,
        weather_for_time[0].get("weather_state_name"),
        weather_for_time[0].get("wind_speed"),
        weather_for_time[0].get("the_temp")))


