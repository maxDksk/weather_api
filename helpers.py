import requests
from os.path import join, dirname
from jsonschema import validate
from datetime import datetime
import json
import pytz

search_by_location_endpoint = "location"
search_by_query_endpoint = "location/search"


def search_by_location(url, location):
    """
    Text to search for.
    :param url:
    :param location: city name (london, san-francisco, etc)
    :return: response object
    """
    return requests.get("{url}/{search_by_query}/?query={location}".format(url=url,
                                                                           search_by_query=search_by_query_endpoint,
                                                                           location=location))


def search_by_latlong(url, lat, long):
    """
    :param url: string
    :param lat: string
    :param long: string
    :return: response object
    """
    return requests.get("{url}/{search_by_query}/?query={lat}, {long}".format(url=url,
                                                                              search_by_query=search_by_query_endpoint,
                                                                              lat=lat, long=long))


def search_by_woeid(url, woeid):
    """
    :param url:
    :param woeid: string
    :return: response object
    """
    return requests.get("{url}/{locat_search}/{woeid}/".format(url=url,
                                                               locat_search=search_by_location_endpoint,
                                                               woeid=woeid))


def search_by_location_date(url, woeid, date):
    """
    :param url: string
    :param woeid: woeid
    :param date: yyyy/mm/dd
    :return: response object
    """
    return requests.get("{url}/{locat_search}/{woeid}/{date}".format(url=url,
                                                                     locat_search=search_by_location_endpoint,
                                                                     woeid=woeid, date=date))


def closest_time(response, time):
    """
    Return dict weather with closest created time to passed
    :param response: json consolidated_weather
    :param time: str %Y/%m/%d %H:%M
    :return: dict
    """
    created_date_time_pattern = "%Y-%m-%dT%H:%M:%S.%f%z"
    time = datetime.strptime(time, "%Y/%m/%d %H:%M")
    utc = pytz.UTC

    return min(
        list(
            sorted(response, key=lambda i: i.get("created"), reverse=True)),
        key=lambda x: abs(
            datetime.strptime(x.get("created"), created_date_time_pattern) - utc.localize(time)))


def get_weather_by_time(response, time):
    """
    :param response: JSON response
    :param time: str time %Y/%m/%d %H:%M
    :return: list with weather for specified time (by hour and minute), empty list if there is no weather for specified time
    """
    created_date_time_pattern = "%Y-%m-%dT%H:%M:%S.%f%z"
    time = datetime.strptime(time, "%Y/%m/%d %H:%M")
    utc = pytz.UTC
    return list(filter(
        lambda weather: datetime.strptime(weather.get("created"), created_date_time_pattern).hour == utc.localize(
            time).hour and
                        datetime.strptime(weather.get("created"), created_date_time_pattern).minute == utc.localize(
            time).minute, response))


def assert_valid_schema(data, schema_file):
    """ Checks whether the given data matches the schema """

    schema = _load_json_schema(schema_file)
    return validate(data, schema)


def _load_json_schema(filename):
    """ Loads the given schema file """

    relative_path = join('schemas', filename)
    absolute_path = join(dirname(__file__), relative_path)

    with open(absolute_path) as schema_file:
        return json.loads(schema_file.read())
